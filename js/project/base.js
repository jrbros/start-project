/* Main js */

$(document).ready(function() {

	// Activation menu
	$('#buttonMenu').click(function() {
		$('.page-wrapper').toggleClass('js-transform');
		$('.header').toggleClass('js-transform');
		$('body').toggleClass('js-mobile-nav-active');
		return false;
	});

	// On désactive le menu lors d'un clic 
	$('#mobileNav').click(function() {
		$('body').removeClass('js-mobile-nav-active');
		$('.page-wrapper').removeClass('js-transform');
		$('.header').removeClass('js-transform');
	});

	// Ajout du header fixed lorqu'on scrolle
	$(window).scroll(function(){

		var positionToTop = $(window).scrollTop();

		if (positionToTop > 0) {
			$('.header').addClass('js-header-fixed');
			$('body').addClass('js-body-margin-top-header-fixed');
		} else  {
			$('.header').removeClass('js-header-fixed');
			$('body').removeClass('js-body-margin-top-header-fixed');
		} 
	});

	// Effet de scroll vertical
	$('.scroll').click(function() {
	    var elementClicked = $(this).attr("href");
	    var destination = $(elementClicked).offset().top;
	    $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination-15}, 500 );
	    return false;
	});

	// Validation formulaire
	$(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); } );

});
