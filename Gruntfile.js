module.exports = function(grunt) {

  // Configuration de Grunt
  grunt.initConfig({
  	pkg: grunt.file.readJSON('package.json'),

    // Serveur localhost
  	connect: {
  		server: {
	      options: {
	        port: 9000,
	        open: true,
	        livereload: 35729,
	        hostname: 'localhost',
	        base: './',
	      }
	  	}
  	},

    // Copie des fichiers pour la version de production
    copy: {
      build: {
        cwd: './',
        src: ['**', '!scss/**', '!html/**', '!pattern/**', '!js/lib/**', '!js/ltie9/**', '!js/project/**', '!node_modules/**', '!Gruntfile.js', '!package.json'],
        dest: 'build',
        expand: true
      }
    },

    // Clean de la version de production
    clean: {
      build: {
        src: ['build']
      },
    },

    // Traitement des fichiers jade
    jade: {
      dev: {
        options: {
          data: {
            debug: true,
          },
          pretty: true,
        },
        files: [{
          expand: true,
          cwd: 'html/pages/',
          src: '*.jade',
          dest: '',
          ext: '.html'
        }]
      },
      build: {
        options: {
          data: {
            debug: false
          },
          pretty: false,
        },
        files: [{
          expand: true,
          cwd: 'html/pages/',
          src: '*.jade',
          dest: 'build',
          ext: '.html'
        }]
      },
    },

    // Traitement des fichiers scss
  	sass: {
      dev: {
        options: {
          style: 'expanded',
          lineNumbers: true,
        },
        files: {
          'css/main.css': 'scss/main.scss',
        }
      },
  		build: {
  			options: {
  				style: 'compressed'
  			},
  			files: [{
  				'build/css/main.css': 'scss/main.scss',
  			}]
  		}
  	},

    autoprefixer: {
      dev: {
        src: 'css/main.css'
      }
    },

    csslint: {
      options: {
        formatter: [{
          id: 'csslint-xml',
          dest: 'report/csslint-xml'
        }]
      }, 
      src: ['css/main.css']
    },

    // Concaténation des fichiers js
  	concat: {
  		basic : {
  			src : [
  				'js/lib/*.js', 
  				'js/project/base.js' 
  			],

  			dest: 'js/main.js'
  		},
      ie9 : {
        src : [
          'js/ltie9/lib/*.js', 
        ],

        dest: 'js/ltie9.js'
      }
  	},

    jshint: {
      files: ['js/project/*.js', '!js/lib/*.js', '!js/ltie9/*.js']
    },

    // Minification des fichiers js
  	uglify: {
  		build: {
        files: {
          'build/js/main.js': 'build/js/main.js',
          'build/js/ltie9.js': 'build/js/ltie9.js'
        }
  		}
  	},

    // Compression des images
  	imagemin: {
  		dynamic: {
  			files: [{
  				expand: true,
  				cwd: 'build/img/',
  				src: ['**/*.{png,jpg,gif}'],
  				dest: 'build/img/'
  			}]
  		}

  	},

    // Suppression du css non utilisé
    uncss: {
      build: {
        options: {
          ignore: [/js/]
        },
        files: {
          'build/css/main.css': ['build/*.html']
        }
      }
    },

    // Minification du css
    cssmin: {
      build: {
        files: {
          'build/css/main.css': 'build/css/main.css'
        }
      }
    },

    css_mqpacker: {
      options: {
        map: true
      },
      main: {
        expand: true,
        cwd: 'css/',
        src: '*.css'
      }
    },

    'ftp-deploy': {
      build: {
        auth: {
          host: 'dev.serveurpi.com',
          port: 21,
          authKey: 'ftp-connect'
        },
        src: 'build',
        dest: '/jeremyrn/publishers-interface',
        exclusions: []
      }
    },

    styleguide: {
      options: {
        template: {
          src: 'styleguide/template/'
        },
        framework: {
          name: 'kss'
        }
      },
      all: {
        files: [{
          'styleguide': 'css/main.css'
        }]
      }
    },

    // On vérifie si il y a des modifications en dev et on recharge la page
  	watch: {

  		options: {
  			livereload: 35729,
  		},

  		scripts: {
  			files: ['js/*.js', 'js/**/*.js'],
  			tasks: ['jshint', 'concat'],
  			options: {
  				spawn: false,
  			},
  		},

  		css: {
  			files: ['scss/*.scss', 'scss/**/*.scss'],
  			tasks: ['sass:dev', 'autoprefixer', 'styleguide'],
  			options: {
  				spawn: false,
  			}
  		},

      jade: {
        files: ['html/**/*.jade'],
        tasks: ['jade:dev'],
        options: {
          spawn: false,
        }
      },

      imagemin: {
        files: ['img/*.{png,jpg,gif}'],
        tasks: ['imagemin'],
        options: {
          spawn: false,
        }
      }, 
  	}

  });

  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-ftp-deploy');
  grunt.loadNpmTasks('grunt-styleguide');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-css-mqpacker');


// Initinialisation
  grunt.registerTask('default', ['jshint', 'concat', 'sass:dev', 'autoprefixer', 'jade:dev', 'styleguide']);

// Localhost et livereload
  grunt.registerTask('server', ['connect', 'watch']);

// Production
  grunt.registerTask('build', ['clean', 'copy', 'sass:build', 'jade:build', 'uncss', 'cssmin', 'uglify']);

}